
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class UpdateRecordInDB {

    static Connection con = null;
    private static Statement stmt;
    PreparedStatement pstmt = null;
    public static String DB_URL = "jdbc:mysql://localhost:3306/emp?useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";  
    public static String DB_USER = "root";
    public static String DB_PASSWORD = "Asal@2018@ihda";


 @BeforeTest
 public void setUp() throws Exception {
	 try{

         String dbClass = "com.mysql.cj.jdbc.Driver";
         Class.forName(dbClass).newInstance();
         Connection con = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
         stmt = con.createStatement();
         }
         catch (Exception e)
         {
               e.printStackTrace();
         }
 }

 @Test
 public void test() {
        try{
        String query = "select * from employee";
        ResultSet res = stmt.executeQuery(query);
    
        while (res.next())
        {
        	   System.out.print(res.getString(1));
               System.out.print("\t" +res.getString(2));
        System.out.print("\t" + res.getString(3)+"\n");
        }
        
        
        String query2="update employee set Name='Amal' where idemployee=2";
        
        int result = stmt.executeUpdate(query2);
        System.out.println("number of records updated is   " +result);
        System.out.println("the quersy====>"+ query2);
        
        ResultSet res1 = stmt.executeQuery(query);
        while (res1.next())
        {
               System.out.print(res1.getString(1));
        System.out.print("\t" + res1.getString(2)+"\n");
        }
        
        } 
        catch(Exception e)
        {
               e.printStackTrace();
        }     
 }

 @AfterTest
 public void tearDown() throws Exception {

        if (con != null) {
        con.close();
        }
 }
}
