import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class JDBCPreparedStatementUpdateExample {

	private static final String DB_DRIVER = "oracle.jdbc.driver.OracleDriver"; //"com.mysql.cj.jdbc.Driver";
	private static final String DB_CONNECTION = "jdbc:mysql://localhost:3306/emp?useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC"; 
	private static final String DB_USER = "root";
	private static final String DB_PASSWORD = "Asal@2018@ihda";
	static Statement stmt = null;

	public static void main(String[] argv) {

		try {

			updateRecordToTable();
			

		} catch (SQLException e) {

			System.out.println(e.getMessage());

		}

	}

	private static void updateRecordToTable() throws SQLException {

		Connection dbConnection = null;
		PreparedStatement preparedStatement = null;

	
		try {
			dbConnection = getDBConnection();
			
			
			 String query = "select * from employee";
		        ResultSet res = stmt.executeQuery(query);
		    
		        while (res.next())
		        {
		               System.out.print(res.getString(1));
		        System.out.print("\t" + res.getString(2)+"\n");
		        }
			
			
		 
	

			
		   	String updateTableSQL = "update employee set Name=? where idemployee=?";
		   	preparedStatement = dbConnection.prepareStatement(updateTableSQL);
		    preparedStatement.setString(1, "mkyong_new_value");
			preparedStatement.setString(2, "1001");
		   	int result = stmt.executeUpdate(updateTableSQL);
	        System.out.println("number of records updated is   " +result);
	        System.out.println("the quersy====>"+ updateTableSQL);
	        
	        ResultSet res1 = stmt.executeQuery(updateTableSQL);
	        while (res1.next())
	        {
	               System.out.print(res1.getString(1));
	        System.out.print("\t" + res1.getString(2)+"\n");
	        }
		   	
		   	
		   	/*
		   	
			preparedStatement = dbConnection.prepareStatement(updateTableSQL);
		    preparedStatement.setString(1, "mkyong_new_value");
			preparedStatement.setString(2, "1001");
					
			// execute update SQL stetement
			int ii=preparedStatement.executeUpdate();
			System.out.print("################"+ii+"\n");

			/*ResultSet res1=preparedStatement.executeQuery();
			while (res1.next())
	        {
	               System.out.print(res1.getString(1));
	        System.out.print("\t" + res1.getString(2)+"\n");
	        }*/
			
			/*
	        
	        ResultSet res1 = preparedStatement.executeQuery(query);
	        while (res1.next())
	        {
	               System.out.print(res1.getString(1));
	        System.out.print("\t" + res1.getString(2)+"\n");
	        }*/
			
			
			System.out.println("Record is updated to DBUSER table!");
			
			 


		} catch (SQLException e) {

			System.out.println(e.getMessage());

		} finally {

			if (preparedStatement != null) {
				preparedStatement.close();
			}

			if (dbConnection != null) {
				dbConnection.close();
			}

		}

	}

	private static Connection getDBConnection() {

		Connection dbConnection = null;

		try {

			Class.forName(DB_DRIVER);

		} catch (ClassNotFoundException e) {

			System.out.println(e.getMessage());

		}

		try {

			dbConnection = DriverManager.getConnection(
                            DB_CONNECTION, DB_USER,DB_PASSWORD);
			
			///////
			stmt = dbConnection.createStatement();
			return dbConnection;

		} catch (SQLException e) {

			System.out.println(e.getMessage());

		}

		return dbConnection;

	}

}