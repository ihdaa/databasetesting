
import java.sql.Connection;
import java.sql.DriverManager;
//import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class TestUpdate {

 Connection connect = null;
 Statement stmt;

 // Database URL.
 String sqldb_url = "jdbc:mysql://localhost:3306/emp?useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&relaxAutoCommit=true";
 
 //Use your database username here.
 String sqldb_uname = "root";
 //Use your database password here.
 String sqldb_pass = "Asal@2018@ihda";

 @BeforeTest
 public void setUp() throws Exception {
  try {
   String dbClass = "com.mysql.cj.jdbc.Driver";//"com.mysql.jdbc.Driver";
   Class.forName(dbClass).newInstance();
  } catch (Exception e) {
   e.printStackTrace();
  }
 }

 @Test
 public void printTableData() {

  try {
   // To Create database connection.
   connect = DriverManager.getConnection(sqldb_url, sqldb_uname, sqldb_pass);

   stmt = connect.createStatement();
   

   //Printing all records of user table after updating record.
   String query = "select * from employee";
            // Get the contents of user table from DB
            ResultSet res = stmt.executeQuery(query);
            // Print the result untill all the records are printed
            // res.next() returns true if there is any next record else returns false
            while (res.next())
            {
                   System.out.println(String.format("%s - %s - %s  ", res.getString(1), res.getString(2), res.getString(3)));
            }
            
            
            String query2="update employee set Name='Amaaal' where idemployee=2";
            
            int result = stmt.executeUpdate(query2);
            System.out.println("number of records updated is" +result);
            System.out.println("the quersy====>"+ query2);
            
            ResultSet res1 = stmt.executeQuery(query);
            while (res1.next())
            {
                   System.out.print(res1.getString(1));
            System.out.print("\t" + res1.getString(2)+"\n");
            }
            
            //insert
            String insertToSQL =" INSERT INTO employee(idemployee, Name, Age) VALUES (4,'ihdaa',24)";
            int rowsAffected= stmt.executeUpdate(insertToSQL);
            System.out.println(rowsAffected);
           
            //delete
            /*
            String sql = "delete from employee where idemployee=4";
            int rowsAffected= stmt.executeUpdate(sql);
            System.out.println(rowsAffected);
            */
            
            
  } catch (Exception e) {
   e.printStackTrace();
  }
 }

 @AfterTest
 public void tearDown() throws Exception {
  // Close database connection.
  if (connect != null) {
   connect.close();
  }
 }
}
