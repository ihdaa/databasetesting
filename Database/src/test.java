import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;


public class test {
	
	 static Connection conn = null;
	 Statement stmt;

	 // Database URL.
	 static String sqldb_url = "jdbc:mysql://localhost:3306/emp?useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&relaxAutoCommit=true";
	 
	 //Use your database username here.
	 static String sqldb_uname = "root";
	 //Use your database password here.
	 static String sqldb_pass = "Asal@2018@ihda";

	 @BeforeTest
	 public void setUp() throws Exception {
	  try {
	   String dbClass = "com.mysql.cj.jdbc.Driver";//"com.mysql.jdbc.Driver";
	   Class.forName(dbClass).newInstance();
	  } catch (Exception e) {
	   e.printStackTrace();
	  }
	 }
	
	
	
	
	
	 @Test
	public static void updateDescriptionAndAuthor (
			 // Connection conn,
			  String Name,
			  int idemployee,
			  int Age,
			  String author
			)
			throws SQLException
			{
			  try
			  {
				  Name="dddd";
				  idemployee=1;
				  Age=22;
				  
				 conn = DriverManager.getConnection(sqldb_url, sqldb_uname, sqldb_pass);
			    // create our java preparedstatement using a sql update query
			    PreparedStatement ps = conn.prepareStatement(
			      "UPDATE employee SET Name = ?, Age = ? WHERE idemployee = ? ");

			    // set the preparedstatement parameters
			    ps.setInt(1,idemployee);
			    ps.setString(2,Name);
			    //ps.setString(2,Age);
			    ps.setInt(3,Age);

			    // call executeUpdate to execute our sql update statement
			    ps.executeUpdate();
			    ps.close();
			  }
			  catch (SQLException se)
			  {
			    // log the exception
			    throw se;
			  }
			}
	 
	 
	 @AfterTest
	 public void tearDown() throws Exception {
	  // Close database connection.
	  if (conn != null) {
		  conn.close();
	  }
	 }
}
