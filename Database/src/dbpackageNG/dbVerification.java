package dbpackageNG;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import dbpackage.ReadPropertyFile;

public class dbVerification {

	static Connection conn = null;
	ReadPropertyFile file = null;

	public ArrayList<String> db(String query, String[] dbColumn)
			throws Throwable {

		try {

			file = new ReadPropertyFile();

			conn = DriverManager.getConnection(file.getConnectionString());
			
			Class.forName("com.mysql.cj.jdbc.Driver");


			Statement stat = conn.createStatement();

			// Send query to db
			ResultSet rs = stat.executeQuery(query);
			/*
			 * java.sql.ResultSetMetaData metaData = rs.getMetaData(); int count
			 * = metaData.getColumnCount(); System.out.println(count);
			 */
			ArrayList<String> values = new ArrayList<>();
			while (rs.next()) {
				for (int i = 0; i < dbColumn.length; i++) {
					values.add(rs.getString(dbColumn[i]));
					System.out.println("Stored Username in DB:" +" " +values);
					
			
				}

			}
			return values;

		} catch (Throwable e) {
			throw e;
		} finally {
			if (conn != null) {
				conn.close();
			}
		}
	}

}
