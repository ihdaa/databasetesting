package dbpackageNG;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

import dbpackage.ReadPropertyFile;

public class Methods {

	WebDriver driver = null;

	@BeforeTest
	public String login() throws Exception

	{
		ReadPropertyFile file = new ReadPropertyFile();
		try {
			
			
			System.setProperty("webdriver.gecko.driver", "C:\\driverfirefox\\geckodriver.exe");
			driver = new FirefoxDriver();
		
			driver.get(file.getUrl());
			driver.findElement(By.id("identifierId")).sendKeys(file.getUsername());
			driver.findElement(By.xpath("/html/body/div[1]/div[1]/div[2]/div[2]/div/div/div[2]/div/div[2]/div/div[1]/div/content/span")).click();
			Thread.sleep(1000);
			
			//String errMsg1 = driver.findElement(By.id("errormsg_0_Email")).getText();
			driver.findElement(By.xpath("/html/body/div[1]/div[1]/div[2]/div[2]/div/div/div[2]/div/div[1]/div/form/content/section/div/content/div[1]/div/div[1]/div/div[1]/input")).sendKeys(file.getPassword());
			driver.findElement(By.xpath("/html/body/div[1]/div[1]/div[2]/div[2]/div/div/div[2]/div/div[2]/div/div[1]/div/content/span")).click();
			Thread.sleep(3000);
			
			String url = driver.getCurrentUrl();
			if (url.contains("https://mail.google.com/mail")) {
				System.out.println("Entered Username on UI: " + file.getUsername());
			} else {
				String errMsg2 = driver.findElement(By.className("error-msg"))
						.getText();
				System.out.println("Unable to Login with:" + file.getUsername()
						+ errMsg2);
				driver.close();
			}

			driver.close();

		} catch (Exception e) {
			System.out.println(e);
		}
		return file.getUsername();

	}
	
	@AfterTest
	public void Aftertest() throws Exception {
		
		driver.quit();
		
	}
}
