package com.via.locators;

import org.openqa.selenium.By;

public class LoginLocators {

	public static By Field_Email=By.id("email");
	public static By Field_Password=By.id("password");
	public static By Button_Login=By.xpath("//div[2]/button");

}
