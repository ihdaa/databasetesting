package com.via.utils;

import java.io.InputStream;
import java.util.Properties;
import com.via.tests.TestBase;


public class ProsperityReader extends TestBase {

	private static Properties properties;

	public static void initializePropertyFiles(String Path) throws Exception{
	
		properties = new Properties();
		InputStream input = null;
	//final String propertiesPath = System.getProperty("user.dir")+"/src/propertiesFiles/";
			input = ProsperityReader.class.getClassLoader().getResourceAsStream(Path);
			if(input==null){
		            System.out.println("Sorry, unable to find propertie file with path :- " + Path);
			    return;
			}
			properties.load(input);		
	}
		

	public static String readPropertiesFile(String ReadData) {

		return (properties.getProperty(ReadData));
		
	}
}
