package com.via.utils;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.chrome.ChromeDriver;
import com.via.tests.TestBase;


public class InitializeBrowser extends TestBase{

	private static String url;
	private static String browser;
	
	public static void CreatBrowser() throws Exception {
		ProsperityReader.initializePropertyFiles("com/via/resources/SetupConfiguration.properties");
	
		browser=ProsperityReader.readPropertiesFile("browser");
		url=ProsperityReader.readPropertiesFile("url");
		
		if (browser.equalsIgnoreCase("Chrome")) {
                                             
		    System.setProperty("webdriver.chrome.driver","C:\\selenium_drivers\\chromedriver.exe");
		    driver = new ChromeDriver();
		    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.manage().window().maximize();
			driver.get(url);
		}  
	}
	
}
