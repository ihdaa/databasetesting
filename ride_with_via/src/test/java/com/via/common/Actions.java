package com.via.common;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import com.via.tests.TestBase;


public class Actions extends TestBase {

	public static void click(By Path) {
		driver.findElement(Path).click();
	}

	public static void sendKeys(By Path, String dataInput) {
		driver.findElement(Path).sendKeys(dataInput);
	}

	public static void EnterKeys(By Path) {
		driver.findElement(Path).sendKeys(Keys.ENTER);
	}
	

}
