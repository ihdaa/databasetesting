package com.via.tests;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import com.via.utils.InitializeBrowser;

public class TestBase {

	public static WebDriver driver;
	

	@BeforeTest()
	public void beforetest() throws Exception {		
		InitializeBrowser.CreatBrowser();
        Thread.sleep(1000);
	}	
	
	@AfterTest()
	public void Aftertest(){
		System.out.println("close the website");
		driver.quit();
		
	}
}
