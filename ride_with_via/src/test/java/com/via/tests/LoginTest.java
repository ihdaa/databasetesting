package com.via.tests;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.via.objectData.UserLoginData;
import com.via.pageImplementation.LoginPageImplementation;

public class LoginTest extends TestBase {

	LoginPageImplementation login;
	UserLoginData userLoginData;
	
	@Test
	@Parameters({"email","password"})
	public void loginTest(String email,String password) throws Exception {
		login=new LoginPageImplementation();
		userLoginData=new UserLoginData(email,password);
		login.Login_Form(userLoginData.getUserEmail(),userLoginData.getPassword());
		
	}
}
