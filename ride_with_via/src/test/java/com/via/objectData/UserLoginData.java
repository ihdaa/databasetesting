package com.via.objectData;

import com.via.pageImplementation.LoginPageImplementation;

public class UserLoginData{

	String userEmail;
    String userPassword;

    public UserLoginData(String userEmail, String password) {
        this.userEmail = userEmail;
        this.userPassword = password;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public String getPassword() {
        return userPassword;
    }
    
    
 /*   public void getUserData(String userEmail,String userPassword) throws Exception {
    //	UserLoginData userLoginData =new UserLoginData(userEmail,userPassword);
    	LoginPageImplementation.Login_Form(getUserEmail(),getPassword());
    
    }*/

}
