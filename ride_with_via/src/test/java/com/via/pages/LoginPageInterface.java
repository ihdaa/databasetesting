package com.via.pages;

public interface LoginPageInterface {

	public void navigate_Login();
	
	public void Login_Form(String email,String password) throws Exception;
}
